package com.testm.partnerssdk_example_android

/**
 * to use TestPartners SDK you will need to configure a few things before you start

 * 1) in file gradle.properties(Project properties) add authToken=(token you will receive from us)

 * 2) in build.gradle (Project) add in maven { url "https://jitpack.io"
                                                credentials { username authToken }
                                           }
      inside allprojects -> repositories  **See build.gradle for example**

 * 3) you will HAVE to open a firebase project(https://console.firebase.google.com) with your package:com.testm.partnerssdk_example_android (in this case)

 * 4) once you implemented the google-services.json file in your project

 * 5) DO NOT implement platform('com.google.firebase:firebase-bom:26.2.0') you will get a duplication error when building your code!
      instead use implementation 'com.google.firebase:firebase-core:(latest version)'

 * 6) go to your project manifest file and add the 'provider' (you can copy it from this projects manifest)

 * 7) in your application class (preferably) initiate TestmLabz to initiate the SDK (See 'MyApp' class for example)

 * 8) on first initiation the SDK will have to download some resources so it might take a few seconds to be ready you can use the percents from callback 'onProcess' to show the process to the user

 * 9) you can determine wether the SDK succesfully loaded via @PARAM isSdkInitialzed if SDK couldnt load you can get the reason via @PARAM error

 */

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.testm.labz.TestmLabz
import com.testm.labz.enums.FlowType
import com.testm.labz.utils.sdk_initializer.ServiceSdkCallback

class MainActivity : AppCompatActivity()
{
    val TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPartnersSDK()
    }

    private fun initPartnersSDK() {

        MyApp.instance.testmLabz.initSDK("14065", object : ServiceSdkCallback {
            override fun onSdkResponse(isSdkInitialzed: Boolean, error: Throwable?) {
                if (isSdkInitialzed) {
                    TestmLabz.startTestmPartners(context = this@MainActivity, flowType = FlowType.Diagnostics)
                } else {
                    Toast.makeText(this@MainActivity, "Error loading SDK", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onProcess(percent: Int) {
                Log.e(TAG, "$percent%")
            }
        })
    }
}