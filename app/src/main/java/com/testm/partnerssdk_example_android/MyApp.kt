package com.testm.partnerssdk_example_android

import android.app.Application
import com.testm.labz.TestmLabz
import com.testm.labz.utils.TestmLabzExtras
import com.testm.labz.utils.helpers.EventCallback


class MyApp : Application()
{
    lateinit var testmLabz: TestmLabz

    override fun onCreate(){
        super.onCreate()
        instance = this
        initPartners()
    }

    private fun initPartners()
    {

        /**if you dont want to use the summary page you can mark shouldHideSummary as true
          * and insert TestmLabzExtras instance into TestMLabz.with

        like so:
        val extras = TestmLabzExtras(shouldHideSummary = true)
               testmLabz =  TestmLabz.with(application = this,testmLabzExtras = extras,...

        */
        testmLabz = TestmLabz.with(application = this,eventCallback =  object : EventCallback {
            override fun onSingleTestStarted(testKey: String)
            {

            }

            override fun onSingleTestFinished(testName: String, isPassed: Boolean) {

            }

            override fun onSdkClosedAfterSubmit(passedTests: List<String>?, failedTests: List<String>?, unfinishedTests: List<String>?)
            {

            }
            override fun onImeiUpdate(newImei: String) {

            }
            override fun onOfferPageError() {

            }
        })

    }

    companion object{
        lateinit var instance: MyApp
            private set
    }
}